		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1.0, width=device-width" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<!--Les balises structurantes de HTML5 sous IE6, IE7 et IE8 -->
			<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
			<![endif]-->
		<!--Le positionnement inline-block sous IE6 et IE7 -->
			<!--[if lte IE 7]>
			<link rel="stylesheet" href="style_ie.css"/>
			<![endif]-->
		<link rel="stylesheet" href="css/style.css" />
		<link rel="icon" type="image/png" href="" />
		<link href='http://fonts.googleapis.com/css?family=Exo+2:300italic,100italic,200italic,200,500italic,300,400,500,100,400italic' rel='stylesheet' type='text/css'>
