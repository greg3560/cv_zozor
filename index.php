<!DOCTYPE html>
<html>
	<head>
		<title>CV ZOZOR</title>
		<?php
			require("inc/head.php");
		?>
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="bloc_page">
			<header>
				<img src="images/photo.png" class="photo_zozor" alt="photo_zozor"/>
				<h1>Célestin Zozor</h1>
				<h2>Intégrateur, mascotte, globe-trotteur et bourrique</h2>
				<nav>
					<ul>
						<li><img src="images/social/viadeo.png" alt="viadeo" class="liens"/><a href="">Mon Viadeo</a></li>
						<li><img src="images/social/twitter.png" alt="twitter" class="liens"/><a href="">Mon Twitter</a></li>
						<li><img src="images/social/facebook.png" alt="facebook" class="liens"/><a href="">Mon Facebook</a></li>
						<li><img src="images/social/linkedin.png" alt="linkedin" class="liens"/><a href ="">Mon Linkedin</a></li>
						<li><img src="images/social/mail.png" alt="mail" class="liens"/><a href="">Mon Mail</a></li>
					</ul>
				</nav>
			</header>
			<section id="competences_bloc">
				<h2><img src="images/titres/competences.png" alt="competences" class="item"/>Compétences</h2>
				<aside id="html">
					<ul>
						<li><h3><img src="images/competences/html5.png" alt="html5" class="competences"/>HTML5<br/></h3><figure class="level5"></figure></li>
						<li><h3><img src="images/competences/xml.png" alt="xml" class="competences"/>XML<br/></h3><figure class="level3"></figure></li>
						<li><h3><img src="images/competences/ps.png" alt="ps" class="competences"/>Photoshop<br/></h3><figure class="level4"></figure></li>
					</ul>
				</aside>
				<aside id="css3">
					<ul>
						<li><h3><img src="images/competences/css3.png" alt="css3" class="competences"/>CSS3<br/></h3><figure class="level5"></figure></li>
						<li><h3><img src="images/competences/xpath.png" alt="xpath" class="competences"/>XPath<br/></h3><figure class="level2"></figure></li>
						<li><h3><img src="images/competences/illustrator.png" alt="illustrator" class="competences"/>Illustrator<br/></h3><figure class="level2"></figure></li>
					</ul>
				</aside>
				<section id="mob">
				<aside id="php">
					<ul>
						<li><h3><img src="images/competences/php.png" alt="php" class="competences"/>PHP<br/></h3><figure class="level2"></figure></li>
						<li><h3><img src="images/competences/javascript.png" alt="javascript" class="competences"/>Javascript<br/></h3><figure class="level3"></figure></li>
					</ul>
				</aside>
				<aside id="mysql">
					<ul>
						<li><h3><img src="images/competences/mysql.png" alt="mysql" class="competences"/>MySql<br/></h3><figure class="level1"></figure></li>
						<li><h3><img src="images/competences/jquery.png" alt="jquery" class="competences"/>Jquery<br/></h3><figure class="level2"></figure></li>
					</ul>
				</aside>
				</section>
			</section>
			<section id="experiences_professionnelles">
				<h2><img src="images/titres/experiences_pro.png" alt="experiences professionnelles" class="experience"/>Expériences professionnelles</h2>
				<img src="images/experiences/simple_it_logo.png" alt="simple it logo" class="experience"/>
				<h3>Depuis 2010 <strong>Intégrateur Web</strong> chez Simple IT, Paris</h3>
				<p>
					Réalisation d'interfaces web riches dans le respect des maquettes fournies par le pôle graphisme.Intégrations valides W3C en HTML5 et CSS3 pour la 
					réalisation d'un site de cours en ligne.Mise en place de la stratégie de responsive web design pour un accès lisible sur tous les mobiles et toutes les tablettes.
				</p>
				<img src="images/experiences/macdo_logo.png" alt="macdo logo" class="experience"/>
				<h3>2009 <strong>Equipier polyvalent</strong> chez Mc Donalds,Avignon</h3>
				<p>
					Tenu de la caisse,réalisation des hamburgers,des frites, des potatoes et de tout un tas d'autres choses bien grasses.Animations Ronald le weekend.
				</p>
				<img src="images/experiences/tavel_logo.png" alt="tavel logo" class="experience"/>
				<h3>2008 <strong>Vendanges</strong> chez Tavel</h3>
				<p>	
					Vendanges du premier rosé de France durant le mois de septembre 2008.
				</p>
			</section>
			<section id="formations_et_diplome">
				<h2><img src="images/titres/formations.png" alt="formation" class="formation" />Formations & diplômes</h2>
				<h3>2003-2008 <strong>diplôme d'ingénieur</strong> à Polytechnique</h3>
				<p>
					Spécialité système d'information.Major de promo.Réalisation du projet de fin d'étude "OpenClassrooms":plateforme communautaire d'éducation ouverte à tous.
				</p>
				<h3>2001-2003 <strong>Baccalauréat scientifique</strong> lycée Henri IV, Paris</h3>
				<p>
					Premier de la classe et délégué de classe chaque année!
				</p>
			</section>
			<section id="centre_d_interet">
				<h2><img src="images/titres/centre_interet.png" alt="centre d'interet" class="hobby"/>Centres d'interêt</h2>
				<aside id="voyage">
					<ul>
						<li><img src="images/centre_interet/voyages.png" alt="voyage" class="centre_interet"/><h3>j'aime les voyages</h3><p>Chaque année, je pars en Asie en Afrique ou en Océanie.</p></li>
						<li><img src="images/centre_interet/tir_a_l_arc.png" alt="tir a l'arc" class="centre_interet"/><h3>Le tir à l'arc</h3><p>Je pratique depuis tout petit et je ne rate jamais ma cible!</p></li>
					</ul>
				</aside>
				<aside id="jeux_video">
					<ul>
						<li><img src="images/centre_interet/jeux.png" alt="jeux" class="centre_interet"/><h3>Les jeux vidéo</h3><p>FPS,RPG,Stratégie...Je suis un expert reconnu de DoTa</p></li>
						<li><img src="images/centre_interet/films.png" alt="films" class="centre_interet"/><h3>Les films</h3><p>Je suis un fan absolu de l'âne dans Shrek (c'est mon cousin).</p></li>
					</ul>
				</aside>
				<aside id="photographie">
					<ul>
						<li><img src="images/centre_interet/photographie.png" alt="photo" class="centre_interet"/><h3>La photographie</h3><p>Je fais des photos de mes nombreux voyages.</p></li>
						<li><img src="images/centre_interet/poulet.png" alt="poulet" class="centre_interet"/><h3>Manger du poulet</h3><p>J'ai fait de cette passion un site www.zozormangedupoulet.eu</p></li>
					</ul>
				</aside>
			</section>
			<footer>
				<h2><img src="images/titres/contact.png" alt="contact" class="contact"/>Contact</h2>
				<form method="post" action="traitement.php" id="formulaire">
					<input type="text" name="nom" placeholder="Votre nom"/>
					<br/>
					<input type="text" name="mail" placeholder="Votre email"/>
					<br/>
					<textarea placeholder="Votre message"></textarea>
					<br/>
					<input type="submit" value="Envoyer" id="envoyer"/>
				</form>
				<aside id="google_map">
					<address>
						28 Boulevard Haussmann<br/>
						75009 Paris<br/>
						Tél:06.06.06.06.06
					</address>
					<iframe src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=28+Boulevard+Haussmann,+Paris,+France&amp;aq=0&amp;oq=28+boulevard&amp;sll=37.0625,-95.677068&amp;sspn=58.076329,114.169922&amp;ie=UTF8&amp;hq=&amp;hnear=28+Boulevard+Haussmann,+75008+Paris,+%C3%8Ele-de-France,+France&amp;t=m&amp;z=14&amp;ll=48.872867,2.335035&amp;output=embed"></iframe>
				</aside>
			</footer>
		</div>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-85159820-11', 'auto');
		  ga('send', 'pageview');

		</script>
	</body>
</html>
